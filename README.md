# CS 250: Basic Data Structures using C++, JCCC

Repository for student projects



# Using Git

## Pulling this repository to your local computer:

git clone URL

(Replace URL with the HTTPS URL from the website)



## Making a backup of changes:

git add *.h

git add *.cpp

git commit -m "description of what you worked on"



## Pulling any changes from the server (do this after a commit; not always required):

git pull



## Pushing changes to the server:

git push



# Ownership info

After the semester is over, feel free to make a FORK of this repository 
into your own GitLab account. You can set the repo to public if you'd 
like, or use any of the code in a portfolio repository, etc. 
-- it's yours.
