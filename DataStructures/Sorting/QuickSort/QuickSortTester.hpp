#ifndef _QUICK_SORT_TESTER_HPP
#define _QUICK_SORT_TESTER_HPP

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "QuickSort.hpp"
#include "../../cutest/TesterBase.hpp"
#include "../../Utilities/StringUtil.hpp"
#include "../../Exceptions/NotImplementedException.hpp"
#include "../../Exceptions/StructureFullException.hpp"
#include "../../Exceptions/InvalidIndexException.hpp"
#include "../../Exceptions/NullptrException.hpp"

//! TESTER for Quick Sort
class QuickSortTester : public TesterBase
{
public:
    QuickSortTester()
        : TesterBase( "test_result_quick_sort.html" )
    {
        AddTest(TestListItem("Test_QuickSort",         bind(&QuickSortTester::Test_QuickSort, this)));
    }

    virtual ~QuickSortTester() { }

private:
    int Test_QuickSort();
};

int QuickSortTester::Test_QuickSort()
{
    string functionName = "QuickSort";
    Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );

        try                                     {   vector<int> blah = { 3, 1, 2 }; QuickSort::Sort( blah ); }
        catch( NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "Create unsorted vector { \"c\", \"b\", \"a\" }, check that sort works." ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        vector<string> data;
        data.push_back( "c" );
        data.push_back( "b" );
        data.push_back( "a" );

        Set_Comments( "Before sort" );
        for ( size_t i = 0; i < data.size(); i++ )
        {
            Set_Comments( "Item " + StringUtil::ToString( i ) + ": \"" + data[i] + "\"" );
        }

        vector<string> expectedOutput = { "a", "b", "c" };

        QuickSort::Sort( data );

        bool allMatch = true;
        for ( size_t i = 0; i < data.size(); i++ )
        {
            Set_ExpectedOutput( "Item " + StringUtil::ToString( i ), expectedOutput[i] );
            Set_ActualOutput( "Item " + StringUtil::ToString( i ), data[i] );

            if ( data[i] != expectedOutput[i] )
            {
                allMatch = false;
            }
        }

        if      ( !allMatch )   { TestFail(); }
        else                    { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "Create unsorted vector { 5, 3, 4, 7, 1 }, check that sort works." ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        vector<int> data;
        data.push_back( 5 );
        data.push_back( 3 );
        data.push_back( 4 );
        data.push_back( 7 );
        data.push_back( 1 );

        Set_Comments( "Before sort" );
        for ( size_t i = 0; i < data.size(); i++ )
        {
            Set_Comments( "Item " + StringUtil::ToString( i ) + ": " + StringUtil::ToString( data[i] ) );
        }

        vector<int> expectedOutput = { 1, 3, 4, 5, 7 };

        QuickSort::Sort( data );

        bool allMatch = true;
        for ( size_t i = 0; i < data.size(); i++ )
        {
            Set_ExpectedOutput( "Item " + StringUtil::ToString( i ), StringUtil::ToString( expectedOutput[i] ) );
            Set_ActualOutput( "Item " + StringUtil::ToString( i ), StringUtil::ToString( data[i] ) );

            if ( data[i] != expectedOutput[i] )
            {
                allMatch = false;
            }
        }

        if      ( !allMatch )   { TestFail(); }
        else                    { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

#endif
