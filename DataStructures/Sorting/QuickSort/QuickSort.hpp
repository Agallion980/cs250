#ifndef _QUICK_SORT_HPP
#define _QUICK_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace QuickSort
{

// Declarations
template <typename T>
void Sort( vector<T>& arr );

template <typename T>
void Sort( vector<T>& arr, int low, int high );

template <typename T>
int Partition( vector<T>& arr, int low, int high );

// Definitions
template <typename T>
void Sort( vector<T>& arr )
{
    Sort( arr, 0, arr.size() - 1 );
}

template <typename T>
void Sort( vector<T>& arr, int low, int high )
{
    throw NotImplementedException( "QuickSort" );
}

template <typename T>
int Partition( vector<T>& arr, int low, int high )
{
    throw NotImplementedException( "HeapSort Partition" );
}

}

#endif
