#ifndef _SELECTION_SORT_HPP
#define _SELECTION_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace SelectionSort
{

template <typename T>
void Sort( vector<T>& arr )
{
    throw NotImplementedException( "SelectionSort" );
}

}

#endif
