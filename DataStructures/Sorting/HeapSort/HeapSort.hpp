#ifndef _HEAP_SORT_HPP
#define _HEAP_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

// Need a binary heap
namespace HeapSort
{

template <typename T>
void Sort( vector<T>& arr )
{
    throw NotImplementedException( "HeapSort" );
}

}

#endif
