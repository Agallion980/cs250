#ifndef _MERGE_SORT_HPP
#define _MERGE_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace MergeSort
{

// Declarations
template <typename T>
void Sort( vector<T>& arr );

template <typename T>
void Sort( vector<T>& arr, int left, int right );

template <typename T>
void Merge( vector<T>& arr, int left, int mid, int right );

// Definitions
template <typename T>
void Sort( vector<T>& arr )
{
    throw NotImplementedException( "MergeSort" );
}

template <typename T>
void Sort( vector<T>& arr, int left, int right )
{
}

template <typename T>
void Merge( vector<T>& arr, int left, int mid, int right )
{
}

}

#endif
