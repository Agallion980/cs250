#include "OmniProgram.hpp"
#include "Utilities/Menu.hpp"

#include "DataStructure/LinkedList/LinkedListTester.hpp"
#include "DataStructure/SmartDynamicArray/SmartDynamicArrayTester.hpp"
#include "DataStructure/SmartFixedArray/SmartFixedArrayTester.hpp"
#include "DataStructure/BinarySearchTree/BinarySearchTreeTester.hpp"
#include "DataStructure/Queue/QueueTester.hpp"
#include "DataStructure/Stack/StackTester.hpp"
#include "DataStructure/HashTable/HashTableTester.hpp"

#include "Sorting/BubbleSort/BubbleSortTester.hpp"
#include "Sorting/HeapSort/HeapSortTester.hpp"
#include "Sorting/InsertionSort/InsertionSortTester.hpp"
#include "Sorting/MergeSort/MergeSortTester.hpp"
#include "Sorting/QuickSort/QuickSortTester.hpp"
#include "Sorting/RadixSort/RadixSortTester.hpp"
#include "Sorting/SelectionSort/SelectionSortTester.hpp"

#include "TestPrograms/Program_SFA.hpp"
#include "TestPrograms/Program_SDA.hpp"
#include "TestPrograms/Program_LL.hpp"
#include "TestPrograms/Program_Stack.hpp"
#include "TestPrograms/Program_Queue.hpp"
#include "TestPrograms/Program_BST.hpp"
#include "TestPrograms/Program_HT.hpp"

#include "DemoPrograms/CollegeCovidProgram.hpp"
#include "DemoPrograms/VetProgram.hpp"

#include "Projects/Project1/AirlineProgram.h"

OmniProgram::OmniProgram()
    : DATA_PATH( "../DemoPrograms/data/" )      // UPDATE THIS TO MATCH YOUR PATHING
{
}

void OmniProgram::MainMenu()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "Aggregated CS250 Topics" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Data structures - run tests",
            "Data structures - run programs",
            "Demos - Run data programs",
            "Projects - run projects",
            "Sorting - run tests"
        }, true, true );

        switch( choice )
        {
            case 0: done = true;                break;
            case 1: DataStructuresTests();      break;
            case 2: DataStructuresPrograms();   break;
            case 3: DemoPrograms();             break;
            case 4: ProjectPrograms();          break;
            case 5: SortingTests();             break;

            default: cout << ":|" << endl;
        }
    }
}

void OmniProgram::DataStructuresTests()
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Tester - Smart fixed array",
        "Tester - Smart dynamic array",
        "Tester - Linked list",
        "Tester - Queues",
        "Tester - Stacks",
        "Tester - BinarySearchTree",
        "Tester - HashTable"
        }, true, true );

    switch( choice )
    {
        case 1:
            {
            SmartFixedArrayTester sfaTester;
            sfaTester.Start();
            break;
            }

        case 2:
            {
            SmartDynamicArrayTester sdaTester;
            sdaTester.Start();
            break;
            }

        case 3:
            {
            LinkedListTester llTester;
            llTester.Start();
            break;
            }

        case 4:
            {
            QueueTester qTester;
            qTester.Start();
            break;
            }

        case 5:
            {
            StackTester sTester;
            sTester.Start();
            break;
            }

        case 6:
            {
            BinarySearchTreeTester bstTester;
            bstTester.Start();
            break;
            }

        case 7:
            {
            HashTableTester htTester;
            htTester.Start();
            break;
            }
    }
}

void OmniProgram::DataStructuresPrograms()
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Program - Smart fixed array",
        "Program - Smart dynamic array",
        "Program - Linked list",
        "Program - Queues",
        "Program - Stacks",
        "Program - BinarySearchTree",
        "Program - HashTable"
        }, true, true );

    switch( choice )
    {
        case 1:
            {
            Program_SFA menu_SFA;
            menu_SFA.Run();
            break;
            }

        case 2:
            {
            Program_SDA menu_SDA;
            menu_SDA.Run();
            break;
            }

        case 3:
            {
            Program_LL menu_LL;
            menu_LL.Run();
            break;
            }

        case 4:
            {
            Program_Stack menu_S;
            menu_S.Run();
            break;
            }

        case 5:
            {
            Program_Queue menu_Q;
            menu_Q.Run();
            break;
            }

        case 6:
            {
            Program_BST menu_BST;
            menu_BST.Run();
            break;
            }

        case 7:
            {
            Program_HT menu_HT;
            menu_HT.Run();
            break;
            }
    }
}

void OmniProgram::DemoPrograms()
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "College Covid data",
        "Vet data",
        }, true, true );

    switch( choice )
    {
        case 1:
            {
            CollegeCovidProgram ccp;
            ccp.Run( DATA_PATH );
            break;
            }

        case 2:
            {
            VetProgram vp;
            vp.Run( DATA_PATH );
            break;
            }
    }
}

void OmniProgram::ProjectPrograms()
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Project 1"
    }, false, true );

    switch( choice )
    {
        case 1: {
            AirlineProgram airlineProgram;
            airlineProgram.Run();
            break;
        }
    }
}

void OmniProgram::SortingTests()
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Tester - Bubble Sort",
        "Tester - Heap Sort",
        "Tester - Insertion Sort",
        "Tester - Merge Sort",
        "Tester - Quick Sort",
        "Tester - Radix Sort",
        "Tester - Selection Sort",
    }, true, true );

    switch( choice )
    {
        case 1:
            {
            BubbleSortTester bsTester;
            bsTester.Start();
            break;
            }

        case 2:
            {
            HeapSortTester hsTester;
            hsTester.Start();
            break;
            }

        case 3:
            {
            InsertionSortTester isTester;
            isTester.Start();
            break;
            }

        case 4:
            {
            MergeSortTester msTester;
            msTester.Start();
            break;
            }

        case 5:
            {
            QuickSortTester qsTester;
            qsTester.Start();
            break;
            }

        case 6:
            {
                cout << "NOT IMPLEMENTED" << endl;
            //RadixSortTester rsTester;
            //rsTester.Start();
            break;
            }

        case 7:
            {
            SelectionSortTester ssTester;
            ssTester.Start();
            break;
            }
    }
}


