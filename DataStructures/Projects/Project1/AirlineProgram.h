#ifndef PROGRAM_H
#define PROGRAM_H

#include "Passenger.h"

#include <queue>
#include <stack>
#include <vector>
#include <fstream>
using namespace std;

class AirlineProgram
{
    public:
    AirlineProgram();
    ~AirlineProgram();

    void Run();

    private:
    void LineUp( const Passenger& passenger );
    void Embark( string time );
    void Disembark( string time );
    void Summary();

    // TODO: Add queue of passengers

    // TODO: Add stack of airplane seats

    // TODO: Add list of arrived passengers

    string NiceTime( int hr, int min );
    void TimePasses( int& hr, float& min );

    ofstream m_log;
    int m_time;
};

#endif // PROGRAM_H
