#ifndef _AUTHOR
#define _AUTHOR

#include <string>
using namespace std;

struct Author
{
    int authorId;
    string name;
};

#endif
