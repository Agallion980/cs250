#ifndef _BOOK
#define _BOOK

#include <string>
using namespace std;

struct Book
{
    unsigned int isbn10;
    int authorId;
    string title;
};

#endif
