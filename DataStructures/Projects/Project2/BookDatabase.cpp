#include "BookDatabase.h"

#include "../../Utilities/CsvParser.hpp"
#include "../../Utilities/StringUtil.hpp"
#include "../../Utilities/Timer.hpp"

#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

BookDatabase::BookDatabase()
{
}

BookDatabase::~BookDatabase()
{
}

void BookDatabase::LoadBooks( string filename )
{
    cout << "Load books from \"" << filename << "\"..." << endl;
    throw NotImplementedException( "BookDatabase::LoadBooks" );    
}

void BookDatabase::LoadAuthors( string filename )
{
    cout << "Load authors from \"" << filename << "\"... ";
    throw NotImplementedException( "BookDatabase::LoadAuthors" );
}

Book BookDatabase::GetBookFromTree( unsigned int isbn )
{
    throw NotImplementedException( "BookDatabase::GetBookFromTree" );
}

Book BookDatabase::GetBookFromList( unsigned int isbn )
{
    throw NotImplementedException( "BookDatabase::GetBookFromList" );
}

Author BookDatabase::GetAuthorFromTree( int id )
{
    throw NotImplementedException( "BookDatabase::GetAuthorFromTree" );
}

Author BookDatabase::GetAuthorFromList( int id )
{
    throw NotImplementedException( "BookDatabase::GetAuthorFromList" );
}

void BookDatabase::SaveCombined( string filename )
{
    throw NotImplementedException( "BookDatabase::SaveCombined" );
}

void BookDatabase::SaveStats()
{
    cout << "Output stats..." << endl;

    /* Uncomment me out later
    auto CalculateAverage = []( vector<unsigned int> stats ) -> float {
        unsigned int sum = 0;
        for ( auto& num : stats ) { sum += num; }
        return sum / stats.size();
    };

    float average_BookTreePush     = CalculateAverage( m_stats_BookTreePush );
    float average_BookListPush     = CalculateAverage( m_stats_BookListPush );
    float average_AuthorTreePush   = CalculateAverage( m_stats_AuthorTreePush );
    float average_AuthorListPush   = CalculateAverage( m_stats_AuthorListPush );
    float average_BookTreeSearch   = CalculateAverage( m_stats_BookTreeSearch );
    float average_BookListSearch   = CalculateAverage( m_stats_BookListSearch );
    float average_AuthorTreeSearch = CalculateAverage( m_stats_AuthorTreeSearch );
    float average_AuthorListSearch = CalculateAverage( m_stats_AuthorListSearch );

    m_stats << endl << "SUMMARY" << endl << endl;

    m_stats << fixed << setprecision( 5 );

    const int W = 15;

    m_stats << left
        << setw( W ) << "DATA"
        << setw( W ) << "FUNCTION"
        << setw( W ) << "TREE AVG MS"
        << setw( W ) << "LIST AVG MS"
        << endl << string( 80, '-' ) << endl;

    m_stats
        << setw( W ) << "Book"
        << setw( W ) << "Push"
        << setw( W ) << average_BookTreePush
        << setw( W ) << average_BookListPush << endl;

    m_stats
        << setw( W ) << "Author"
        << setw( W ) << "Push"
        << setw( W ) << average_AuthorTreePush
        << setw( W ) << average_AuthorListPush << endl;

    m_stats
        << setw( W ) << "Book"
        << setw( W ) << "Search"
        << setw( W ) << average_BookTreeSearch
        << setw( W ) << average_BookListSearch << endl;

    m_stats
        << setw( W ) << "Author"
        << setw( W ) << "Search"
        << setw( W ) << average_AuthorTreeSearch
        << setw( W ) << average_AuthorListSearch << endl;
  */
}


