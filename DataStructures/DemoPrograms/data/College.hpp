#ifndef _COLLEGES_HPP
#define _COLLEGES_HPP

#include <vector>
#include <string>
using namespace std;

// Structure in the same format as the data from the csv file
// date,state,county,city,ipeds_id,college,cases,cases_2021,notes
struct College
{
    string date;
    string state;
    string county;
    string city;
    string ipeds_id;
    string college;
    int cases;
    int cases_2021;
    string notes;

    void Set( const vector<string>& header, const vector<string>& row );

    friend bool operator==( const College& left, const College& right );
    friend bool operator!=( const College& left, const College& right );
    friend bool operator<( const College& left, const College& right );
    friend bool operator>( const College& left, const College& right );
    friend bool operator<=( const College& left, const College& right );
    friend bool operator>=( const College& left, const College& right );

    friend ostream& operator<<( ostream& out, const College& item );
};

#endif
