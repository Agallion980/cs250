#include "College.hpp"

#include "../../Utilities/StringUtil.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

void College::Set( const vector<string>& header, const vector<string>& row )
{
    for ( unsigned int i = 0; i < row.size(); i++ )
    {
        if      ( header[i] == "date" )         { date = row[i]; }
        else if ( header[i] == "state" )        { state = row[i]; }
        else if ( header[i] == "county" )       { county = row[i]; }
        else if ( header[i] == "city" )         { city = row[i]; }
        else if ( header[i] == "ipeds_id" )     { ipeds_id = row[i]; }
        else if ( header[i] == "college" )      { college = row[i]; }
        else if ( header[i] == "cases" )        { cases = StringUtil::StringToInt( row[i] ); }
        else if ( header[i] == "cases_2021" )   { cases_2021 = StringUtil::StringToInt( row[i] ); }
        else if ( header[i] == "notes" )        { notes = row[i]; }
    }
}

bool operator==( const College& left, const College& right )
{
    return ( left.college == right.college );
}

bool operator!=( const College& left, const College& right )
{
    return !( left == right );
}

bool operator<( const College& left, const College& right )
{
    return ( left.college < right.college );
}

bool operator>( const College& left, const College& right )
{
    return ( left.college > right.college );
}

bool operator<=( const College& left, const College& right )
{
    return ( left < right || left == right );
}

bool operator>=( const College& left, const College& right )
{
    return ( left > right || left == right );
}

//    string date;
//    string state;
//    string county;
//    string city;
//    string ipeds_id;
//    string college;
//    int cases;
//    int cases_2021;
//    string notes;

ostream& operator<<( ostream& out, const College& item )
{
    const int W = 80/5;
    out << item.college << endl
        << left
        << setw( 10 ) << "* CASES:" << setw( W ) << item.cases
        << setw( 10 ) << "* STATE:" << setw( W ) << item.state
        << setw( 10 ) << "* CITY:" << setw( W ) << item.city
        << endl;
    return out;
}
