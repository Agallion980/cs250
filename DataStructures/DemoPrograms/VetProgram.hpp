#ifndef _VET_PROGRAM_HPP
#define _VET_PROGRAM_HPP

#include "data/AnimalIntake.hpp"

class VetProgram
{
    public:
    ~VetProgram();

    void Run( string dataPath );
    void Menu_Search();
    void Menu_Sort();
    void ViewAllData();

    private:
    void Init( string dataPath );
    void Clean( vector<AnimalIntake*> cleanme );
    vector<AnimalIntake*> Duplicate( const vector<AnimalIntake*> original );

    bool m_done;
    vector<AnimalIntake*> m_allData;
};

#endif
