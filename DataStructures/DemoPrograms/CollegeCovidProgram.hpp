#ifndef _COLLEGE_COVID_PROGRAM_HPP
#define _COLLEGE_COVID_PROGRAM_HPP

#include "data/College.hpp"

class CollegeCovidProgram
{
    public:
    ~CollegeCovidProgram();

    void Run( string dataPath );
    void Menu_Search();
    void Menu_Sort();
    void ViewAllData();

    private:
    void Init( string dataPath );
    void Clean( vector<College*> cleanme );
    vector<College*> Duplicate( const vector<College*> original );

    bool m_done;
    vector<College*> m_collegeData;
};

#endif
