#ifndef _OMNI_PROGRAM
#define _OMNI_PROGRAM

#include <string>
using namespace std;

class OmniProgram
{
    public:
    OmniProgram();
    void MainMenu();
    void DataStructuresTests();
    void DataStructuresPrograms();
    void DemoPrograms();
    void ProjectPrograms();
    void SortingTests();

    private:
    const string DATA_PATH;
};

#endif
