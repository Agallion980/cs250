#ifndef PROGRAM_SDA_HPP
#define PROGRAM_SDA_HPP

#include "../DataStructure/SmartDynamicArray/SmartDynamicArray.hpp"

//! A simple program to test the SmartDynamicArray
class Program_SDA
{
    public:
    void Run();

    private:
    SmartDynamicArray<int> m_data;
};

#endif
