#ifndef PROGRAM_BST_HPP
#define PROGRAM_BST_HPP

#include "../DataStructure/BinarySearchTree/BinarySearchTree.hpp"

//! A simple program to test the BinarySearchTree
class Program_BST
{
    public:
    void Run();

    private:
    BinarySearchTree<int, char> m_data;
};

#endif
