#include "Program_BST.hpp"

#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Logger.hpp"

#include "../Exceptions/InvalidIndexException.hpp"
#include "../Exceptions/ItemNotFoundException.hpp"
#include "../Exceptions/NotImplementedException.hpp"
#include "../Exceptions/NullptrException.hpp"
#include "../Exceptions/StructureEmptyException.hpp"

#include <cstdlib>
#include <ctime>

void Program_BST::Run()
{
    srand( time( NULL ) );

    ofstream eventLog( "eventlog-bst.txt" );

    for ( int i = 0; i < 1000; i++ )
    {
        int random = rand() % 6;

        try
        {
            switch( random )
            {
                case 0:     // Push
                {
                    int randomValue = rand() % 65 + 25;
                    char letter = static_cast<char>( randomValue );
                    eventLog << "Push( " << randomValue << ", " << letter << " )" << endl;
                    cout << "Push( " << randomValue << ", " << letter << " )" << endl;
                    m_data.Push( randomValue, letter );
                } break;
                case 1:     // Contains
                {
                    int randomValue = rand() % 65 + 25;
                    eventLog << "Contains( " << randomValue << " ) = ";
                    cout << "Contains( " << randomValue << " ) = ";
                    bool result = m_data.Contains( randomValue );
                    eventLog << result << endl;
                    cout << result << endl;
                } break;
                case 2:    // GetData
                {
                    int randomValue = rand() % 65 + 25;
                    eventLog << "GetData( " << randomValue << " ) = ";
                    cout << "GetData( " << randomValue << " ) = ";
                    char& result = m_data.GetData( randomValue );
                    eventLog << result << endl;
                    cout << result << endl;
                } break;
                case 3:     // GetInOrder, GetPreOrder, GetPostOrder
                {
                    eventLog    << "GetInOrder() = ";
                    cout        << "GetInOrder() = ";
                    string result = m_data.GetInOrder();
                    eventLog    << result << endl;
                    cout        << result << endl;

                    eventLog    << "GetPreOrder() = ";
                    cout        << "GetPreOrder() = ";
                    result = m_data.GetPreOrder();
                    eventLog    << result << endl;
                    cout        << result << endl;

                    eventLog    << "GetPostOrder() = ";
                    cout        << "GetPostOrder() = ";
                    result = m_data.GetPostOrder();
                    eventLog    << result << endl;
                    cout        << result << endl;

                } break;
                case 4:     // GetMinKey, GetMaxKey, GetHeight, GetCount
                {
                    eventLog    << "GetMinKey() = ";
                    cout        << "GetMinKey() = ";
                    int result = m_data.GetMinKey();
                    eventLog    << result << endl;
                    cout        << result << endl;

                    eventLog    << "GetMaxKey() = ";
                    cout        << "GetMaxKey() = ";
                    result = m_data.GetMaxKey();
                    eventLog    << result << endl;
                    cout        << result << endl;

                    eventLog    << "GetHeight() = ";
                    cout        << "GetHeight() = ";
                    result = m_data.GetHeight();
                    eventLog    << result << endl;
                    cout        << result << endl;

                    eventLog    << "GetCount() = ";
                    cout        << "GetCount() = ";
                    result = m_data.GetCount();
                    eventLog    << result << endl;
                    cout        << result << endl;

                } break;
            }
        }
        catch( const NotImplementedException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( ... )
        {
            eventLog << "EXCEPTION: Misc" << endl;
            cout << "EXCEPTION: Misc" << endl;
        }
    }

    eventLog.close();
}

