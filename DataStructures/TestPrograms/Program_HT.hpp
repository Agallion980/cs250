#ifndef PROGRAM_HT_HPP
#define PROGRAM_HT_HPP

#include "../DataStructure/HashTable/HashTable.hpp"

//! A simple program to test the HashTable
class Program_HT
{
    public:
    void Run();

    private:
    HashTable<char> m_data;
};

#endif
