#ifndef PROGRAM_SFA_HPP
#define PROGRAM_SFA_HPP

#include "../DataStructure/SmartFixedArray/SmartFixedArray.hpp"

//! A simple program to test the SmartFixedArray
class Program_SFA
{
    public:
    void Run();

    private:
    SmartFixedArray<int> m_data;
};

#endif
