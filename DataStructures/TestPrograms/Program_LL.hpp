#ifndef PROGRAM_LL_HPP
#define PROGRAM_LL_HPP

#include "../DataStructure/LinkedList/LinkedList.hpp"

//! A simple program to test the LinkedList
class Program_LL
{
    public:
    void Run();

    private:
    LinkedList<int> m_data;
};

#endif
