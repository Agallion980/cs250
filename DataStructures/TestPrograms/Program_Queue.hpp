#ifndef PROGRAM_Q_HPP
#define PROGRAM_Q_HPP

#include "../DataStructure/Queue/ArrayQueue.hpp"
#include "../DataStructure/Queue/LinkedQueue.hpp"

//! A simple program to test the Queue
class Program_Queue
{
    public:
    void Run();

    private:
    ArrayQueue<int> m_dataArr;
    LinkedQueue<int> m_dataLink;
};

#endif
