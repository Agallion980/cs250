#include "Program_HT.hpp"

#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Logger.hpp"

#include "../Exceptions/InvalidIndexException.hpp"
#include "../Exceptions/ItemNotFoundException.hpp"
#include "../Exceptions/NotImplementedException.hpp"
#include "../Exceptions/NullptrException.hpp"
#include "../Exceptions/StructureEmptyException.hpp"

#include <cstdlib>
#include <ctime>

void Program_HT::Run()
{
    srand( static_cast<size_t>( time( NULL ) ) );

    ofstream eventLog( "eventlog-bst.txt" );

    for ( int i = 0; i < 100; i++ )
    {
        int random = rand() % 3;

        try
        {
            switch( random )
            {
                case 0:     // Push
                {
                    int randomValue = rand() % 65 + 25;
                    char letter = static_cast<char>( randomValue );
                    eventLog << "Push( " << randomValue << ", " << letter << " )" << endl;
                    cout << "Push( " << randomValue << ", " << letter << " )" << endl;
                    m_data.Push( randomValue, letter );
                } break;
                case 1:     // SetCollisionMethod
                {
                    int randomValue = rand() % 3;
                    eventLog << "SetCollisionMethod( " << randomValue << " )" << endl;
                    cout << "SetCollisionMethod( " << randomValue << " )" << endl;
                    CollisionMethod cm = static_cast<CollisionMethod>( randomValue );
                    m_data.SetCollisionMethod( cm );
                } break;
                case 2:    // Get
                {
                    int randomValue = rand() % 65 + 25;
                    eventLog << "Get( " << randomValue << " ) = ";
                    cout << "Get( " << randomValue << " ) = ";
                    try
                    {
                        char& result = m_data.Get( randomValue );
                        eventLog << result << endl;
                        cout << result << endl;
                    }
                    catch( ... )
                    {
                        eventLog << "NOT FOUND" << endl;
                        cout << "NOT FOUND" << endl;
                    }
                } break;
            }
        }
        catch( const NotImplementedException& ex )
        {
            eventLog << "EXCEPTION: " << ex.what() << endl;
            cout << "EXCEPTION: " << ex.what() << endl;
        }
        catch( ... )
        {
            eventLog << "EXCEPTION: Misc" << endl;
            cout << "EXCEPTION: Misc" << endl;
        }
    }

    eventLog.close();
}

