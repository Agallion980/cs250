#include "Utilities/Logger.hpp"
#include "OmniProgram.hpp"

int main()
{
    Logger::Setup();

    OmniProgram program;
    program.MainMenu();

    Logger::Cleanup();

    return 0;
}







