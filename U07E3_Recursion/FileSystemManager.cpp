#include "FileSystemManager.hpp"

#include "utilities/Logger.hpp"
#include "utilities/StringUtil.hpp"

/****************************************************************************************/
/* SEARCH FOR FILE/FOLDER                                                              */
/****************************************************************************************/

/**
Public entry-point to begin searching for a file.
Starts by calling Recursive_FindFile and passing in the ROOT folder and the name of the file to find.
*/
File* FileSystemManager::FindFile( string name )
{
    Logger::Out( "Find file: " + name, "FileSystemManager::FindFile" );

    // Begin searching at root
    return Recursive_FindFile( m_allFolders[0], name );
}

/**
Public entry-point to begin searching for a folder.
Starts by calling Recursive_FindFile and passing in the ROOT folder and the name of the folder to find.
*/
Folder* FileSystemManager::FindFolder( string name )
{
    Logger::Out( "Find folder: " + name, "FileSystemManager::FindFolder" );

    // Begin searching at root
    return Recursive_FindFolder( m_allFolders[0], name );
}

/**
Recursive find function for files.

1. Search through all files in the folder ptrFolder.
    - If there is a filename match, return the pointer to that file.
    - Otherwise if the file name doesn't match anything, go to next step.

2. Recurse through all the subfolders to search for the file. Store the result in a pointer.
    - If the pointer is not nullptr, the file was found - return the same result.
    - Once the loop is done, we can assume that the file wasn't found in any subfolders - return nullptr.
*/
File* FileSystemManager::Recursive_FindFile( Folder* ptrFolder, string name )
{
    Logger::Out( "Searching folder: " + ptrFolder->GetName(), "FileSystemManager::Recursive_FindFile" );

    // TODO:    Iterate through all files that belong to the current folder (ptrFolder).
    //          If the name matches, return the pointer to that file.
    for ( auto& ptr : ptrFolder->GetFiles() )
    {
        if ( ptr->GetName() == name )
        {
            return ptr;
        }
    }



    // TODO:    Iterate through all subfolders that belong to the current folder (ptrFolder).
    //          Recurse into each subfolder. If the result is found, return it.
    //          Otherwise, once the loop is finished, return nullptr.
    for ( auto& ptr : ptrFolder->GetFolders() )
    {
        File* result = Recursive_FindFile( ptr, name );
        if ( result != nullptr )
        {
            return result;
        }
    }

    return nullptr;     // File not found
}

/**
Recursive find function for folders.

1. If this folder's name matches the name we're searching for, return this pointer (ptrFolder).

2. Otherwise, iterate through all of this folder's subfolders, recurse into each one, storing the result in a pointer.
    - If the result is not nullptr, return the result pointer.
    - Once the loop is done, we can assume that the folder was not found in any subfolders. Return nullptr.
*/
Folder* FileSystemManager::Recursive_FindFolder( Folder* ptrFolder, string name )
{
    Logger::Out( "Searching folder: " + ptrFolder->GetName(), "FileSystemManager::Recursive_FindFolder" );

    // TODO:    Check to see if this folder's name matches the name passed in. If so, return this folder (ptrFolder).



    // TODO:    Iterate through all subfolders that belong to the current folder (ptrFolder).
    //          Recurse into each subfolder. If the result is found, return it.
    //          Otherwise, once the loop is finished, return nullptr.




    return nullptr;     // Folder not found
}


/****************************************************************************************/
/* DISPLAY FILE/FOLDER                                                                  */
/****************************************************************************************/

/**
Public entry-point to begin displaying the filesystem.
Starts by calling Recursive_DisplayFolder and passing in the ROOT folder and the starting level of the directory (0).
*/
void FileSystemManager::Display()
{
    Logger::Out( "Display filesystem", "FileSystemManager::Display" );

    // Start at root
    Recursive_DisplayFolder( m_allFolders[0], 0 );
}

/**
Indents and then displays the data of the file passed in.
*/
void FileSystemManager::DisplayFile( File* ptrFile, int level )
{
    Logger::Out( "Display file: " + ptrFile->GetName(), "FileSystemManager::Recursive_DisplayFile" );

    // Display self
    Indent( level );
    ptrFile->Display();
    cout << endl;
}

/**
Indents and then displays the name of the folder passed in.

Afterwards, it will iterate through all subfolders and recurse into those.

Afterwards, it will iterate through all files and display each file's information.
*/
void FileSystemManager::Recursive_DisplayFolder( Folder* ptrFolder, int level )
{
    Logger::Out( "Display folder: " + ptrFolder->GetName(), "FileSystemManager::Recursive_DisplayFolder" );

    // Display self
    Indent( level );
    ptrFolder->Display();
    cout << endl;

    // Display subfolders
    for ( auto& ptr : ptrFolder->GetFolders() )
    {
        Recursive_DisplayFolder( ptr, level+1 );
    }

    // Display subfiles
    for ( auto& ptr : ptrFolder->GetFiles() )
    {
        DisplayFile( ptr, level+1 );
    }
}

/****************************************************************************************/
/* ALREADY IMPLEMENTED                                                                  */
/****************************************************************************************/

/**
Sets up the mock filesystem
*/
FileSystemManager::FileSystemManager()
{
    // Create files and folders and set up
    // which files go in what folders and so on.
    Folder* root        = new Folder( "home" );                     m_allFolders.push_back( root );

    Folder* folder1     = new Folder( "school" );                   m_allFolders.push_back( folder1 );      root->AddFolder( folder1 );
    Folder* folder1a    = new Folder( "cs235" );                    m_allFolders.push_back( folder1a );     folder1->AddFolder( folder1a );
    Folder* folder1b    = new Folder( "cs210" );                    m_allFolders.push_back( folder1b );     folder1->AddFolder( folder1b );

    Folder* folder2     = new Folder( "work" );                     m_allFolders.push_back( folder2 );      root->AddFolder( folder2 );
    Folder* folder2a    = new Folder( "dayjob" );                   m_allFolders.push_back( folder2a );     folder2->AddFolder( folder2a );
    Folder* folder2b    = new Folder( "sidegig" );                  m_allFolders.push_back( folder2b );     folder2->AddFolder( folder2b );
    Folder* folder2ba   = new Folder( "billing" );                  m_allFolders.push_back( folder2ba );    folder2b->AddFolder( folder2ba );

    Folder* folder3     = new Folder( "hobbies" );                  m_allFolders.push_back( folder3 );      root->AddFolder( folder3 );
    Folder* folder3a    = new Folder( "basketweaving" );            m_allFolders.push_back( folder3a );     folder3->AddFolder( folder3a );
    Folder* folder3b    = new Folder( "constructed-languages" );    m_allFolders.push_back( folder3b );     folder3->AddFolder( folder3b );

    Folder* folder4     = new Folder( "files" );                    m_allFolders.push_back( folder4 );      root->AddFolder( folder4 );

    File* file;
    file         = new File( "homework1.odt",    "1. Learn polymorphism, 2. Get A" );   m_allFiles.push_back( file );     folder1a->AddFile ( file );
    file         = new File( "homework2.odt",    "!(p && q) = !p || !q" );              m_allFiles.push_back( file );     folder1b->AddFile ( file );
    file         = new File( "transcript.odt",   "Got all the good grades @ jccc" );    m_allFiles.push_back( file );     folder1->AddFile  ( file );
    file         = new File( "references.odt",   "Person who isn't family: 111-2222" ); m_allFiles.push_back( file );     folder1->AddFile  ( file );
    file         = new File( "resume.odt",       "Worked job at place, 2019" );         m_allFiles.push_back( file );     folder2->AddFile  ( file );
    file         = new File( "logfile.txt",      "Everything crashed" );                m_allFiles.push_back( file );     folder2a->AddFile ( file );
    file         = new File( "complaints.csv",   "Bob microwaves fish" );               m_allFiles.push_back( file );     folder2a->AddFile ( file );
    file         = new File( "posting.txt",      "Software Engineer, $75,000/yr" );     m_allFiles.push_back( file );     folder2b->AddFile ( file );
    file         = new File( "january.csv",      "Co1 owes me $100" );                  m_allFiles.push_back( file );     folder2ba->AddFile( file );
    file         = new File( "february.csv",     "Co1 still hasn't paid" );             m_allFiles.push_back( file );     folder2ba->AddFile( file );
    file         = new File( "march.csv",        "Still waiting on Co1's payment" );    m_allFiles.push_back( file );     folder2ba->AddFile( file );
    file         = new File( "todo.txt",         "Learn to draw, learn piano" );        m_allFiles.push_back( file );     folder3->AddFile( file );
    file         = new File( "done.txt",         "Mastered basketweaving!" );           m_allFiles.push_back( file );     folder3->AddFile( file );
    file         = new File( "coolbaskets.jpg",  "u U v V" );                           m_allFiles.push_back( file );     folder3a->AddFile( file );
    file         = new File( "esperanto.txt",    "Chu vi parolas?" );                   m_allFiles.push_back( file );     folder3b->AddFile( file );
    file         = new File( "ido.txt",          "Ka vu parolas?" );                    m_allFiles.push_back( file );     folder3b->AddFile( file );
    file         = new File( "tokipona.txt",     "sina pilin seme?" );                  m_allFiles.push_back( file );     folder3b->AddFile( file );
    file         = new File( "laadan.txt",       "BAa thal ne?" );                      m_allFiles.push_back( file );     folder3b->AddFile( file );
    file         = new File( "notpirated.mp4",   "The Lost Skeleton of Cadavra" );      m_allFiles.push_back( file );     folder4->AddFile( file );
    file         = new File( "notavirus.exe",    "DeleteHarddrive();" );                m_allFiles.push_back( file );     folder4->AddFile( file );
    file         = new File( "screenshot.png",   "(>o.o)>----| (o_o)" );                m_allFiles.push_back( file );     folder4->AddFile( file );

    Logger::Out( StringUtil::ToString( m_allFiles.size() ) + " files created", "FileSystemManager::FileSystemManager" );
    for ( unsigned int i = 0; i < m_allFiles.size(); i++ )
    {
        Logger::Out( "File " + StringUtil::ToString( i ) + ": " + m_allFiles[i]->GetName(), "FileSystemManager::FileSystemManager" );
    }

    Logger::Out( StringUtil::ToString( m_allFolders.size() ) + " folders created", "FileSystemManager::FileSystemManager" );
    for ( unsigned int i = 0; i < m_allFolders.size(); i++ )
    {
        Logger::Out( "Folder " + StringUtil::ToString( i ) + ": " + m_allFolders[i]->GetName(), "FileSystemManager::FileSystemManager" );
    }
}

/**
Frees memory for all the allocated files and folders
*/
FileSystemManager::~FileSystemManager()
{
    for ( unsigned int i = 0; i < m_allFolders.size(); i++ )
    {
        Logger::Out( "Delete folder " + m_allFolders[i]->GetName(), "FileSystemManager::~FileSystemManager" );
        delete m_allFolders[i];
    }

    for ( unsigned int i = 0; i < m_allFiles.size(); i++ )
    {
        Logger::Out( "Delete file " + m_allFiles[i]->GetName(), "FileSystemManager::~FileSystemManager" );
        delete m_allFiles[i];
    }
}

/**
Indents by some amount based on level
*/
void FileSystemManager::Indent( int level )
{
    for ( int i = 0; i < level; i++ )
    {
        cout << "--";
    }
    cout << " ";
}
