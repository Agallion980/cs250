#ifndef _FILE_SYSTEM_MANAGER
#define _FILE_SYSTEM_MANAGER

#include "File.hpp"
#include "Folder.hpp"

#include <vector>
using namespace std;

class FileSystemManager
{
    public:
    //! Initializes the FileSystemManager
    FileSystemManager();
    //! Cleans up the FileSystemManager
    ~FileSystemManager();

    //! Display the filesystem structure
    void Display();
    //! Public-facing method for beginning a search for a file
    File*   FindFile( string name );
    //! Public-facing method for beginning a search for a folder
    Folder* FindFolder( string name );

    private:
    //! Display a file's information
    void DisplayFile( File* ptrFile, int level );
    //! Display a folder, recursive
    void Recursive_DisplayFolder( Folder* ptrFolder, int level );

    //! FindFile recursive method, recurses into itself to search subfolders for a file
    File* Recursive_FindFile( Folder* ptrFolder, string name );
    //! FindFolder recursive method, recurses into itself to search subfolders for a folder
    Folder* Recursive_FindFolder( Folder* ptrFolder, string name );

    //! Utility to indent items by a level for easier reading of the filesystem display
    void Indent( int level );
    //! A list of pointers to all files in the directory
    vector<File*>    m_allFiles;
    //! A list of pointers to all folders in the directory
    vector<Folder*>  m_allFolders;
};

#endif
